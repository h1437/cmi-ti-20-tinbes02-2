#ifndef FAT_HPP
#define FAT_HPP

#define MAXFILENAMESIZE 12
#define MAXFILES 10
#define EE_NOOFFILES 0
#define EE_STARTOFFAT 1
#define MAXDATASIZE 32

//#define DEBUG

#include <Arduino.h>
#include <EEPROM.h>

typedef struct
{
    char fileName[MAXFILENAMESIZE];
    int  startAddr;
    int  size;
    
} fatEntry;

//FAT r/w 
fatEntry readFatEntry(int index);
char* readData(int index, int size);
bool writeFatEntry(int index, fatEntry file);
bool writeData(char* data, int startAddr, int size);


//FAT list and manipulations
bool clearFat();
int listFreespace();
void files();
bool storeFile(char *fileName, byte *data, int size);
bool readFile(char *fileName);
bool eraseFile(char *fileName);

//helpers
int findFirstEmptpySpot();
int findNextStartSpot(int i);
int findEmptyDataSpot(int size);
int checkForExistingFile(char *fileName);

#endif
