#include "stack.hpp"

bool hasPeeked = false;

void pushByte(stackType* stack, byte data)
{
    stack->stack[(stack->sp)++] = data;
    //Serial.println(F("Pushed the data to the stack!"));
}

byte popByte(stackType* stack)
{
    return stack->stack [--(stack->sp)];
}

void pushChar(stackType* stack, char c)
{
    pushByte(stack, c);
    pushByte(stack, CHAR);
    //Serial.print(c);
    //Serial.println(F(" pushed CHAR to stack!"));
}

char popChar(stackType* stack)
{
    //popByte(stack);
    char retVal = char(popByte(stack));
    return retVal;
}

void pushInt(stackType* stack, int i)
{
    //assign i on stack with correct endianness TODO:check dit
    pushByte(stack, lowByte(i));
    pushByte(stack, highByte(i));
    pushByte(stack, INT);
    //Serial.print(i);
    //Serial.println(F(" pushed INT to the stack"));
}

int popInt(stackType* stack)
{
    //popByte(stack);
    byte l = popByte(stack);
    byte h = popByte(stack);

    //merge popped low and high bytes back into one integer
    int retVal = word(l, h);

    return retVal;
}

void pushFloat(stackType* stack, float f)
{
    //divide IEEE754 floating points up in 4 seperate bytes
    byte *b = (byte *)&f;

    for (int i = 3; i >= 0; i--)
    { 
        pushByte(stack, b[i]);
        //Serial.println((int)&b[i]);
    }

   /* for (int i = 0; i > 4; i++)
    { 
        pushByte(stack, byteArr[i]);
        Serial.println((int)&byteArr[i]);
    }*/

    pushByte(stack, FLOAT);
    //Serial.print(f,5);
    //Serial.println(F(" pushed FLOAT to the stack!"));
}


float popFloat(stackType* stack)
{
    float f = 0.0;
    byte *b = (byte *)&f;

    //popByte(stack);

    for (int i = 0; i <=3; i++)
    {
        b[i] = popByte(stack);
    }

    return f;
}

int mystrlen(char *p)
{
    int c=0;
    while(*p!='\0')
    {
        c++;
        *p++;
    }
    return(c);
}

// WAARSCHUWING: Dit is een compleet ranzig stukje code
void pushString(stackType* stack, char *str)
{
    byte length = mystrlen(str) + 1;

	pushByte(stack, *str);
	
	//Serial.println(length);

    for (byte i=0; i < length; i++)
    {
        pushByte(stack, str[length-i-1]);
    }
	
    pushByte(stack, length);
    pushByte(stack, STRING);
}

char* popString(stackType* stack, bool popGeneric=true)
{
	if(popGeneric){
		byte genericData = popByte(stack);
	}

	byte length = (byte)(popByte(stack));

	char* s;
    s = (char*)malloc(length);

	for (int i = 0; i < length; i++) {
        s[i] = (char)popByte(stack);
    }
	

    return (char*)s;
}

float peek(stackType* stack)
{
    hasPeeked = true;
    return popVal(stack);
}

float popVal(stackType* stack)
{
    //pop one value from the stack to check what it is
    //then push it back in order to retain it if nessescary
    float f;
    byte genericData = popByte(stack);
    pushByte(stack, genericData);

    switch (genericData)
    {
    case CHAR:
        f = (float)popChar(stack);

        if (hasPeeked)
        {
            pushChar(stack, (char)f);
            hasPeeked = false;
        }
        break;
    
    case INT:
        f = (float)popInt(stack);

        if (hasPeeked)
        {
            pushInt(stack, (int)f);
            hasPeeked = false;
        }
        break;
    
    case FLOAT:
        f = popFloat(stack);
        if (hasPeeked)
        {
            //pushFloat(stack, f);
        }
        break;
	
	case STRING:
		f = NULL;
		
		if (hasPeeked)
        {
            pushByte(stack, STRING);
        }
		
        
    default:
        break;
    }

    return f;
}
