#include "fat.hpp"

EERef numberOfFiles = EEPROM[EE_NOOFFILES];

int endOfFAT;

fatEntry readFatEntry(int index)
{
    fatEntry file;
    EEPROM.get(EE_STARTOFFAT + index * sizeof(fatEntry), file);
    return file;
}

char* readData(int index, int size)
{
    //CHANGE: no new keyword but plain var
    //char *data = new char[size];
    //TODO: this will cause a memory leak if not handled with free()
    //is not used for now
    char *data = malloc(sizeof *data);

    int nextFileAddress = 0;

    for (int i = 0; i < size; i++)
    {
        data[i] = EEPROM.read(index + nextFileAddress);
        nextFileAddress = nextFileAddress + sizeof(char);

#ifdef DEBUG
        Serial.print(data[i]);
#endif
    }
    return data;
}

bool writeFatEntry(int index, fatEntry file)
{
    EEPROM.put(EE_STARTOFFAT + index * sizeof(fatEntry), file);
    //Serial.println(F("FAT entry successfully written!"));
    return true;
}

bool writeData(byte* data, int startAddr, int size)
{
    int nextFileAddress = 0;
    for (int i = 0; i < size; i++)
    {
        EEPROM.write(startAddr + nextFileAddress, data[i]);
        nextFileAddress = nextFileAddress + sizeof(data[i]);
    }
    //Serial.println(F("Data successfully written!"));  
    return true;
}

bool clearFat()
{
    numberOfFiles = EE_NOOFFILES;

    for (byte i = 0; i < MAXFILES; i++)
    {
        fatEntry nullFile = (fatEntry)
        {
            "",
            0,
            0,
        };

        writeFatEntry(i, nullFile);
    }
    return true;
}

int listFreespace()
{
    if (numberOfFiles == 10) return -1;
    int startPosition = EE_STARTOFFAT + sizeof(numberOfFiles) + (sizeof(fatEntry) * MAXFILES);
    int totalFileSize = 0;
    
    for (byte i = 0; i < MAXFILES; i++){
      fatEntry file = readFatEntry(i);
      totalFileSize += file.size;
    }
    
    int emptySpace = EEPROM.length() - (startPosition + (MAXDATASIZE * MAXFILES));
    
    return  EEPROM.length() - totalFileSize -startPosition -emptySpace;
}

void files()
{

    bool filesPresent = false;
    fatEntry file;

    for (int i = 0; i < MAXFILES; i++)
    {
        file = readFatEntry(i);

        if (file.size != 0)
        {
            filesPresent = true;     
            Serial.print(file.fileName);
            Serial.print(F("\t- "));
            Serial.print(F("Entry: "));
            Serial.print(i);
            Serial.print(F("\t- "));
            Serial.print(F("Start address: "));
            Serial.print(file.startAddr);
            Serial.print(F("\t- "));
            Serial.print(F("End address: "));
            Serial.print(file.startAddr + file.size-1);
            Serial.print(F("\t- "));
            Serial.print(F("File size: "));
            Serial.print(file.size);
            Serial.print(F("B"));
            Serial.print(F("\n"));
        }
    }  
    if (!filesPresent)
    {
        Serial.println(F("No files in file system!"));
    }
}

bool storeFile(char *fileName, byte *data, int size)
{
    if (numberOfFiles >= MAXFILES)
    {
        Serial.println(F("Error: maximum amount of file entries reached"));
        return false;
    }

    //TODO: implement file exists check
    if (checkForExistingFile(fileName) != -1)
    {
        Serial.println(F("Error: file already exists within filesystem!"));
        return false;
    }

    int fileStart = findEmptyDataSpot(size);

    if (fileStart == -1)
    {
        Serial.println(F("Error: not enough space available on filesystem!"));
        return false;
    }

    fatEntry file = (fatEntry) 
    {
     "",
     fileStart,
     size   
    };
    
    strcpy(file.fileName, fileName);  

    Serial.print(file.fileName);
    Serial.print(F("\t- "));
    Serial.print(F("Start address: "));
    Serial.print(file.startAddr);
    Serial.print(F("\t- "));
    Serial.print(F("End address: "));
    Serial.print(file.startAddr + file.size-1);
    Serial.print(F("\t- "));
    Serial.print(F("File size: "));
    Serial.print(file.size);
    Serial.println(F("B"));

    writeFatEntry(findFirstEmptpySpot(), file);
    writeData(data, file.startAddr, size); //CHANGE -> met dot
    
    numberOfFiles = numberOfFiles + 1;
    
    return true;
}

/***********************************************************************/

bool readFile(char *fileName)
{
    int index = checkForExistingFile(fileName);

    if (index < 0)
    {
        return false;
    }

    fatEntry file = readFatEntry(index);
    
    for (int i = 0; i < file.size ; i++)
    {

        Serial.print(EEPROM[file.startAddr + i], DEC);
        Serial.print(" ");

        if (i > 0 && i != file.size-1 && i%10 == 0)
        {
            Serial.print(F("\n"));
        }
    }
    Serial.print(F("\n"));
    return  true;   
}

/***********************************************************************/

bool eraseFile(char *fileName)
{
    int index = checkForExistingFile(fileName);

    Serial.print(F("FSDF "));
    Serial.println(index);
    
    if (index >= 0)
    {
        fatEntry file = readFatEntry(index);

        file.fileName[0] = '\0';
        file.startAddr = NULL;
        file.size = 0;

        writeFatEntry(index, file);
        numberOfFiles = numberOfFiles -1;

        return true;
    }
    else
    {
        return false;
    }
}

/*----------------------------------------------------------------------------------------*/

int findFirstEmptpySpot()
{
    for (byte i = 0; i < MAXFILES; i++)
    {
        fatEntry file = readFatEntry(i);
        if (file.size == 0) 
        {
            return i;
        }
    }
    return -1;
}

int findNextStartSpot(int i)
{
    
    for (byte j = i + 1; j < MAXFILES; j++)
    {
        fatEntry file = readFatEntry(i);
        if (file.size != 0)
        {
            return file.startAddr;
        }
    }
    
    return (EEPROM.length() + 1);
}


int findEmptyDataSpot(int size)
{
    int firstAvailableSpot = sizeof(numberOfFiles) + (sizeof(fatEntry) * MAXFILES) + 1;
    
    if (numberOfFiles == 0)
    {
        return firstAvailableSpot;
    }else if(numberOfFiles == 1){
        fatEntry file = readFatEntry(0);
        return firstAvailableSpot + file.size;
    }
    else
    {
        for (byte i = MAXFILES-1; i > 0; i--)
        {
            fatEntry file = readFatEntry(i);
            if (file.startAddr != 0){
                return (file.startAddr+ file.size);
            }
        }
        return -1;
    }
}


int checkForExistingFile(char *fileName)
{
    for (byte i = 0; i < MAXFILES; i++)
    {
        fatEntry file = readFatEntry(i);
        
        if (strcmp(file.fileName, fileName) == 0)
            return i;
    }
    return -1;
}
