#include "cli.hpp"
#include "proc.hpp"

#define BUFSIZE 174
#define CMDSIZE 10
#define MAXFILENAMESIZE 8

const static char program[] PROGMEM = "\
Hi, I would like to HELPSIZE tell you a bit about myself.\n\
";

char inputVal[CMDSIZE];
bool inputComplete = false;

char arg1[MAXFILENAMESIZE];
char arg2[BUFSIZE];

typedef struct commandStructure
{
    char name[CMDSIZE];
    void (*func) ();
} commandType;

/**************************************************************************/

const static commandType command[]
//TODO: dit ding neemt asosciaal veel ram in gebruik, allicht help van entry strippen en in PROGMEM gooien
{
    {"list", &list},
    {"procs", &procs},
    {"add", &add},
    {"store", &store},
    {"retrieve", &retrieve},
    {"erase", &erase},
    {"run", &run},
    {"suspend", &suspend},
    {"kill", &kill},
    {"resume", &resume},
    {"freespace", &freespace},
    {"clear", &clear},
    {"cat", &cat},
    {"rm", &rm},
    {"mem", &availableMemory}
};

/**************************************************************************/

void clearSerialBuffer()
{
    //delayMicroseconds(1042);

    // 1042Ms = 1042000Us
    // 16MHz = 62,5Us wait time per clock cycle
    // 1042000/62,5 = 16672 clock cycles wait time 

    for (int i = 0; i < 16672; i++)
    {
        __asm__("nop\n\t");
    }
}

/**************************************************************************/

void serialEvent()
{
    char cliInput;
    static byte index = 0;

    while (Serial.available())
    {
        clearSerialBuffer();
        cliInput = Serial.read();

        if (cliInput == ';' || cliInput == '\n')
        {
            Serial.print(F(">>>> Command entered: ")); Serial.println(inputVal);        
            index = 0;
            inputComplete = true;
        }
        else
        {
            inputVal[index] = cliInput;
            index++;
            
            if (index >= BUFSIZE)
            {
                Serial.println(F("SYSTEM_ERROR: Command overflow"));
                index = -1;
            }
        }

        if (inputComplete)
        {
            processCliInput();
        }
        clearSerialBuffer();
    }
}

void splitArgs(){
  char inputValNew[BUFSIZE];
  char delim[] = " ";

  strcpy(inputValNew, strtok(inputVal, delim));
  strcpy(arg1, strtok(NULL, delim));
  strcpy(arg2, strtok(NULL, ""));

  char last_char = ' ';
  for(byte i=0; i < BUFSIZE; i++){

    char new_char = arg2[i];
    
    if(last_char == '\\' && new_char =='n'){
      arg2[i-1] = ' ';
      arg2[i] = '\n';
    }
    last_char = new_char;
  }
    
  strcpy (inputVal, inputValNew);
}

/**************************************************************************/

void processCliInput(){

    splitArgs();
#ifdef true
    Serial.print(F("DEBUG: Arg1 = "));
    Serial.println(arg1);
    Serial.print(F("DEBUG: Arg2 = "));
    Serial.println(arg2);
#endif

    static int n = sizeof(command) / sizeof(commandType); 
    for (int i=0; i < n; i++)
    {
      if(strcmp(inputVal, command[i].name ) == 0){
        command[i].func();
      }
    }
    memset(inputVal, 0, sizeof inputVal);
    inputComplete = false;
}

/**************************************************************************/

void procs(){listProcesses();}

void store()
{
    int formattedSize = strlen(arg2) + 1;
    //storeFile(file name, data, size)
    if (storeFile(arg1, arg2, formattedSize))
    {
        //Serial.println(F("Data writen to filesystem: "));
    }
    else
    {
        Serial.print(F("Error: failed to store: "));
        Serial.println(arg1);
    }    
}

/**************************************************************************/

void retrieve()
{    
    if(readFile(arg1))
    {
        Serial.println(F("<<<<"));
    }
    else
    {
        Serial.println(F("Error: file could not be read!"));
    }
}

/**************************************************************************/
void list()
{
    files();
}

/**************************************************************************/

// Add process to process table
void add()
{
    switch(start(arg1))
    {
      case(1):
        //Serial.print(F("Process added to memory: "));
        //Serial.print(arg1);
        break;
      case(-1):
        Serial.println(F("Too many processes"));
        break;
      case(-2):
        Serial.println(F("File does not exist"));
        break;
    }
}
void run()
{
    //if (strlen(arg1) != 0){
    //  add();
   // }
    runPrograms();
}

/**************************************************************************/


void suspend()
{
	Serial.print(F("Suspend: "));
	Serial.println(arg1);
	setProcessState((byte)arg1, 's');
}

/**************************************************************************/
void kill()
{
	Serial.print(F("Kill pid: "));
	Serial.println(arg1);
	setProcessState((byte)arg1, 't');
}

/**************************************************************************/

void resume()
{
	Serial.print(F("Resume: "));
	Serial.println(arg1);
	setProcessState(byte(arg1), 'r');
}

/**************************************************************************/

void clear()
{
    for(int i =0; i < 80; i++){
      Serial.println(F("\n"));
    }
}

/**************************************************************************/

void erase()
{    
    //Serial.println(F("Select a to delete the entire FAT (WARNING: this will delete the entire FAT) \nSelect e to delete a single file \n\nMake your selection: "));
    delay(1000);

    if (strcmp(arg1, "-a") == 0)
    {
        if (clearFat())
        {
            Serial.println(F("Successfully wiped FAT!"));
        }
        else
        {
            Serial.println(F("Error: FAT could not be wiped"));
        }
    }
    else
    {
        if (eraseFile(arg1))
        {
            Serial.println(F("Successfully deleted the file!"));
        }
        else
        {
            Serial.println(F("Error: The file could not be deleted!"));
        }
        
    }
}

/**************************************************************************/

void freespace()
{
    int getFreeSpace = listFreespace();

    if (getFreeSpace == 0)
    {
        Serial.println(F("Memory full!"));
    }
    else if(getFreeSpace == -1)
    {
        Serial.println(F("Memory full!"));
    }
    else
    {
        Serial.print(F("The filesystem has "));
        Serial.print(getFreeSpace);
        Serial.println(F("B available"));
    }
}

void cat(){ retrieve();}
void rm(){ erase();}

// free RAM check for debugging. SRAM for ATmega328p = 2048Kb.
// Stolen from stack overflow
void availableMemory() {
    // Use 1024 with ATmega168
    int size = 2048;
    byte *buf;
    while ((buf = (byte *) malloc(--size)) == NULL);
        free(buf);
    Serial.print(F("Memory use: "));
    Serial.print(2048-size);
    Serial.print(F("bytes "));

    Serial.print((2048-size)/ 2048.0 * 100);
    Serial.println(F("%"));
    
}
