#ifndef CLI_HPP
#define CLI_HPP

#define DEBUG false

#include <Arduino.h>
#include "fat.hpp"

void clearSerialBuffer();
void printAvailableCmds();
void serialEvent();
void processCliInput();
bool assignCommand();

void splitArgs();

void store();
void add();
void procs(); // list all processes
void retrieve();
void list();
void run();
void suspend();
void kill();
void resume();
void erase();
void freespace();
void clear();
void cat();
void rm();
byte getSizeArgs (char * s);
void availableMemory();

#endif
